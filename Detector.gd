extends RayCast2D

func _process(delta):
	# Sets the position of the sound effect to cast_to
	$Sound.position = cast_to
	
	# if collision is detected at cast_to position
	if(is_colliding()):
	
		# change position of rectangle to the collision wall
		var coll_point = get_collision_point()
		$Sound.position = to_local(coll_point)
