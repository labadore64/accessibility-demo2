extends KinematicBody2D

var direction = Vector2.ZERO				# The direction the player is moving
var current_speed = 120;					# How fast the player moves

var old_position = Vector2.ZERO				# The last position you were last frame
var old_distance = 0;						# Distance travelled last frame
var current_distance = 0					# Distance travelled this frame
var distance_total = 0;						# How much distance has been covered since last footstep
var distance_max = 40						# How much distance must be travelled to 

# This is mostly movement code
func _physics_process(delta):
	# Get the controller/keyboard input strength and direction.
	direction.x = Input.get_action_strength("move_right") - Input.get_action_strength("move_left")
	direction.y = Input.get_action_strength("move_down") - Input.get_action_strength("move_up")
	
	# If input is digital, normalize it for diagonal movement
	if abs(direction.x) == 1 and abs(direction.y) == 1:
		direction = direction.normalized()
		
	# Get vector for movement by multiplying speed by direction
	var movement = current_speed * direction
	# Stores the old position
	old_position = position;
	
	# Actually moves the player
	move_and_slide(movement, Vector2(0,-1))
	
	# Stores the old distance from last frame
	old_distance = current_distance;
	# Gets the distance from the old position to the updated position
	current_distance = old_position.distance_to(position)

	# If there was an attempt at movement this frame
	if direction.x != 0 || direction.y != 0:	
		
		# If previous distance and current distance is less than 1
		if(old_distance < 1 && current_distance < 1):
			
			# Play the bonk sound effect, if its not currently playing
			if !$Bonk.playing:
				$Bonk.play()
	
	# Adds the current distance to the amount of distance travelled
	distance_total += current_distance

	# If distance total is greater than distance max
	if distance_total > distance_max:
		
		# Reset the distance total by using modulo
		distance_total = fmod(distance_total,distance_max)
		
		# Play the step sound effect
		$Footstep.play()


# Triggered when a player enters an area2D, like a AreaLabel or near an NPC.
func _on_LabelDetector_area_entered(area):

	# if the area's parent has the property "access_name"
	if "access_name" in area:

		# You want to stop the TTS before reading anything
		TTS.stop()
		# Read off the Area2D's access name
		TTS.speak(area.access_name)
